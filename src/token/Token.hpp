#pragma once

#include <string>



class Token {

public:
    enum Type {
        WHITESPACE,
        COLON,
        SEMICOLON,
        COMMA,
        OPERATOR,
        WORD,
        NUMBER,
        STRING,
    };

    Type getType() const;
    std::string toString() const;

    static std::string typeToString(Type type);

protected:
    Token(Type type);

private:
    Type type;

};
