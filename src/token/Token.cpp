#include "Token.hpp"

#include <sstream>



Token::Type Token::getType() const {
    return this->type;
}

std::string Token::toString() const {
    return "<Token: type=" + typeToString(this->type) + ">";
}

std::string Token::typeToString(Token::Type type) {
    switch (type) {
        case Type::WHITESPACE: return "WHITESPACE";
        case Type::COLON:      return "COLON";
        case Type::SEMICOLON:  return "SEMICOLON";
        case Type::COMMA:      return "COMMA";
        case Type::OPERATOR:   return "OPERATOR";
        case Type::WORD:       return "WORD";
        case Type::STRING:     return "STRING";
        case Type::NUMBER:     return "NUMBER";
        default:               return "INVALID";
    }
}

Token::Token(Type type) {
    this->type = type;
}
