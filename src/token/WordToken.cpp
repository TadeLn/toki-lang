#include "WordToken.hpp"



WordToken::WordToken(std::string content)
    : WordToken() {
    this->content = content;
}

WordToken::WordToken() : Token(Type::WORD) {}
