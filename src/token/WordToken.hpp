#pragma once

#include "Token.hpp"



class WordToken : public Token {

public:
    std::string content;
    WordToken(std::string content);
    WordToken();

};
