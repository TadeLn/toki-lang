#include "OperatorToken.hpp"


char OperatorToken::getChar() const {
    return this->c;
}

OperatorToken::OperatorToken(char c) : Token(Type::OPERATOR) {
    this->c = c;
}