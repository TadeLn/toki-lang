#pragma once

#include "Token.hpp"



class OperatorToken : public Token {

public:
    char getChar() const;
    OperatorToken(char c);

private:
    char c;

};
