#pragma once

#include "Token.hpp"



class StringToken : public Token {

public:
    std::string content;
    StringToken(std::string content);
    StringToken();

};
