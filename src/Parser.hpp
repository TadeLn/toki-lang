#pragma once

#include <memory>
#include <vector>
#include <filesystem>

#include "token/Token.hpp"



class Parser {

public:
    static bool isWhitespace(char c);
    static bool isSmallLetter(char c);
    static bool isBigLetter(char c);
    static bool isLetter(char c);
    static bool isNumber(char c);
    static bool isWordChar(char c);
    static bool isOperator(char c);
    static bool isTokenTerminator(char c);

    static std::unique_ptr<Token> readToken(const std::string& sourceCode, unsigned long& currentIndex);
    static std::unique_ptr<std::vector<std::unique_ptr<Token>>> parseSourceCode(const std::string& sourceCode);
    static std::unique_ptr<std::vector<std::unique_ptr<Token>>> parseFile(const std::filesystem::path& filepath);
};
