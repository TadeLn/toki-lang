#pragma once

#include <iostream>
#include <vector>

#include "token/Token.hpp"
#include "expression/Expression.hpp"



class Interpreter {

public:
    int run();

    Interpreter(std::unique_ptr<const std::vector<std::unique_ptr<Token>>> tokens);

protected:
    std::unique_ptr<const std::vector<std::unique_ptr<Token>>> tokens;
    std::size_t i;

    /*
        bool nextCb(const Token*)
            is called on every new token
            should return true if the token should be accepted, and false if it should be rejected

        bool isDoneCb()
            is called on every new token
            should return true if the reading has ended successfully, then the entire function ends
            should return false if the reading should continue

        void abortCb()
            is called once at failure
            called if nextCb throws an exception or if nextCb returns false
    */ 
    template<typename T, typename U, typename V>
    bool read(T nextCb, U isDoneCb, V abortCb) {
        std::size_t rollback = i;

        for (; i < tokens->size(); i++) {
            if (isDoneCb()) {
                #ifdef DEBUG
                std::cout << "  Done\n";
                #endif

                return true;
            }

            #ifdef DEBUG
            std::cout << "  Analyzing token #" << i << "/" << tokens->size() << ": " << tokens->at(i)->toString() << "\n";
            #endif

            try {
                if (!nextCb(tokens->at(i).get())) break;

                #ifdef DEBUG
                std::cout << "  Accept\n";
                #endif
                
            } catch (std::exception& e) {
                break;
            }
        }

        #ifdef DEBUG
        std::cout << "  Abort\n";
        #endif
        
        abortCb();
        i = rollback;
        return false;
    }

    /*
        bool nextCb(std::unique_ptr<Token>)
            is called on every new token
            has 1 argument: 
            should return true if the token should be accepted, and false if it should be rejected

        bool isDoneCb()
            is called on every new token
            should return true if the reading has ended successfully, then the entire function ends
            should return false if the reading should continue
    */ 
    template<typename T, typename U>
    bool read(T nextCb, U isDoneCb) {
        return read(nextCb, isDoneCb, [](){});
    }

    bool read_tokens(std::unique_ptr<std::vector<std::unique_ptr<Token>>> tokens);
    bool read_symbol(std::string symbolName);
    bool read_expression(std::unique_ptr<Expression>& ex);

    bool read_o_toki_e_ni();

};
