#include "Parser.hpp"

#include <iostream>
#include <fstream>
#include <memory>
#include <stdexcept>

#include "token/ColonToken.hpp"
#include "token/CommaToken.hpp"
#include "token/NumberToken.hpp"
#include "token/SemicolonToken.hpp"
#include "token/StringToken.hpp"
#include "token/OperatorToken.hpp"
#include "token/WhitespaceToken.hpp"
#include "token/WordToken.hpp"



bool Parser::isWhitespace(char c) {
    return (c == ' ' || c == '\n' || c == '\t');
}

bool Parser::isSmallLetter(char c) {
    return 'a' <= c && c <= 'z';
}

bool Parser::isBigLetter(char c) {
    return 'A' <= c && c <= 'Z';
}

bool Parser::isLetter(char c) {
    return isSmallLetter(c) || isBigLetter(c);
}

bool Parser::isNumber(char c) {
    return '0' <= c && c <= '9';
}

bool Parser::isWordChar(char c) {
    return isLetter(c) || isNumber(c) || c == '_';
}

bool Parser::isOperator(char c) {
    return c == '+' ||
        c == '-' ||
        c == '*' ||
        c == '/';
}

bool Parser::isTokenTerminator(char c) {
    return c == ':' ||
        c == ';' ||
        c == ',' ||
        isWhitespace(c) ||
        isOperator(c);
}



std::unique_ptr<Token> Parser::readToken(const std::string& sourceCode, unsigned long& current_index) {
    std::unique_ptr<Token> token = std::unique_ptr<Token>();
    bool escaping = false;

    for (; current_index < sourceCode.length(); current_index++) {
        char c = sourceCode[current_index];

        if (!token) {
            if (c == ':') {
                token = std::make_unique<ColonToken>();
                current_index++;
                break;

            } else if (c == ';') {
                token = std::make_unique<SemicolonToken>();
                current_index++;
                break;

            } else if (c == ',') {
                token = std::make_unique<CommaToken>();
                current_index++;
                break;

            } else if (isOperator(c)) { // [TODO] include more operators
                token = std::make_unique<OperatorToken>(c);
                current_index++;
                break;

            } else if (isNumber(c)) {
                token = std::make_unique<NumberToken>();

            } else if (isWhitespace(c)) {
                token = std::make_unique<WhitespaceToken>();
                current_index++;
                break;

            } else if (c == '"') {
                token = std::make_unique<StringToken>();
                continue;

            } else {
                token = std::make_unique<WordToken>();
            }
        }

        if (token->getType() == Token::Type::NUMBER) {
            NumberToken* numberToken = static_cast<NumberToken*>(token.get());
            if (isTokenTerminator(c)) break;
            if (isNumber(c)) {
                numberToken->content.push_back(c);
            } else {
                throw std::runtime_error(std::string() + "invalid token: found " + c + " in a NUMBER token");
            }

        } else if (token->getType() == Token::Type::WORD) {
            WordToken* wordToken = static_cast<WordToken*>(token.get());
            if (isTokenTerminator(c)) break;
            if (isWordChar(c)) {
                wordToken->content.push_back(c);
            } else {
                throw std::runtime_error(std::string() + "invalid token: found " + c + " in a WORD token");
            }

        } else if (token->getType() == Token::Type::STRING) {
            StringToken* stringToken = static_cast<StringToken*>(token.get());
            if (escaping) {
                if (c == 'n') {
                    stringToken->content.push_back('\n');
                } else if (c == 't') {
                    stringToken->content.push_back('\t');
                } else {
                    stringToken->content.push_back(c);
                }
                escaping = false;

            } else {
                if (c == '"') {
                    current_index++;
                    break;
                } else if (c == '\\') {
                    escaping = true;
                } else {
                    stringToken->content.push_back(c);
                }
            }
        }
    }
    return token;
}



std::unique_ptr<std::vector<std::unique_ptr<Token>>> Parser::parseSourceCode(const std::string& sourceCode) {
    unsigned long index = 0;
    auto tokens = std::make_unique<std::vector<std::unique_ptr<Token>>>();

    while (index < sourceCode.length()) {
        #ifdef DEBUG
        std::cout << "Position #" << index << ": "; // [TODO] show line numbers
        #endif
        try {
            std::unique_ptr<Token> token = readToken(sourceCode, index);
            #ifdef DEBUG
            std::cout << token->toString() << "\n";
            #endif
            tokens->push_back(std::move(token));
        } catch (std::exception& e) {
            std::cerr << "parser error: " << e.what() << "\n";
            return std::make_unique<std::vector<std::unique_ptr<Token>>>();
        }
    }
    return tokens;
}



std::unique_ptr<std::vector<std::unique_ptr<Token>>> Parser::parseFile(const std::filesystem::path& filepath) {
    std::ifstream file;
    file.open(filepath);
    if (!file.is_open()) {
        throw std::runtime_error("File could not be opened");
    }

    std::string content((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));

    #ifdef DEBUG
    const long length = ((std::string)filepath).length();
    std::cout << "Processing file:\n"
    "+" << std::string(length + 4, '-') << "+\n"
    "|  " << filepath << "  |\n"
    "+" << std::string(length + 4, '-') << "+\n"
    << content << "\n"
    << std::string(length + 6, '.') << "\n";
    #endif
    return parseSourceCode(content);
}
