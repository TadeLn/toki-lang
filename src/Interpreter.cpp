#include "Interpreter.hpp"

#include <iostream>
#include <stdexcept>

#include "token/OperatorToken.hpp"
#include "token/StringToken.hpp"
#include "token/WordToken.hpp"
#include "token/ColonToken.hpp"
#include "expression/LiteralExpression.hpp"
#include "expression/OperationExpression.hpp"



int Interpreter::run() {
    for (i = 0; i < tokens->size(); i++) {
        #ifdef DEBUG
        std::cout << "Token #" << i << "/" << tokens->size() << ": " << tokens->at(i)->toString() << "\n";
        #endif

        if (tokens->at(i)->getType() == Token::Type::WHITESPACE) {
            continue;
        }

        if (read_o_toki_e_ni()) {
            std::unique_ptr<Expression> ex;
            if (!read_expression(ex)) {
                throw std::runtime_error("expected expression after 'o toki e ni:'");
            }
            std::cout << ex->eval()->toString();
            continue;
         }

         throw std::runtime_error("invalid syntax");
    }
    return 0;
}

Interpreter::Interpreter(std::unique_ptr<const std::vector<std::unique_ptr<Token>>> tokens) {
    this->tokens = std::move(tokens);
    this->i = 0;
}



bool Interpreter::read_tokens(std::unique_ptr<std::vector<std::unique_ptr<Token>>> correctTokens) {
    unsigned int x = 0;
    return read([&](const Token* t) {
        Token::Type type = t->getType();
        if (type == Token::Type::WHITESPACE) {
            return true;
        }
        if (type == correctTokens->at(x)->getType()) {

            // If the token is a WordToken, check if the actual word content is the same
            if (type == Token::Type::WORD) {
                const WordToken* actualWordToken = static_cast<const WordToken*>(t);
                const WordToken* correctWordToken = static_cast<const WordToken*>(correctTokens->at(x).get());
                if (actualWordToken->content != correctWordToken->content) {
                    return false;
                }
            }

            x++;
            return true;
        }
        return false;
    }, [&]() {
        return x == correctTokens->size();
    });
}

bool Interpreter::read_symbol(std::string symbolName) {
    bool done = false;

    return read([&](const Token* t){
        if (t->getType() == Token::Type::WHITESPACE) {
            return true;
        }

        if (t->getType() == Token::Type::WORD) {
            const WordToken* wordToken = static_cast<const WordToken*>(t);
            symbolName = wordToken->content;
            done = true;
            return true;
        }

        return false;
    }, [&](){
        return done;
    });
}

bool Interpreter::read_expression(std::unique_ptr<Expression>& resultExpression) {
    bool done = false;
    bool expectingOperator = false; // Expecting an operator or a semicolon (end of expression) now
    auto none = OperationExpression::Operation::NONE;
    OperationExpression::Operation currentOperation = none; // Current operation

    return read([&](const Token* t){
        Token::Type type = t->getType();
        if (type == Token::Type::WHITESPACE) {
            return true;
        }

        if (expectingOperator) {
            if (type == Token::Type::SEMICOLON) {
                done = true;
                return true;

            // [TODO] add more operators
            } else if (type == Token::Type::OPERATOR) {
                const OperatorToken* operatorToken = static_cast<const OperatorToken*>(t);

                if (operatorToken->getChar() == '+') {
                    expectingOperator = false;
                    currentOperation = OperationExpression::Operation::ADD;
                    return true;
                }

            } else if (type == Token::Type::WORD) {
                const WordToken* wordToken = static_cast<const WordToken*>(t);

                if (wordToken->content == "en") {
                    expectingOperator = false;
                    currentOperation = OperationExpression::Operation::ADD;
                    return true;
                }
            }

        } else {
            if (type == Token::Type::STRING) {
                // [TODO] add more expression types (variables, function calls, etc.)
                const StringToken* stringToken = static_cast<const StringToken*>(t);
                std::unique_ptr<Expression> newExpression = std::make_unique<LiteralExpression>(Value::from(stringToken->content));

                if (currentOperation == none) {
                    resultExpression = std::move(newExpression);
                } else {
                    resultExpression = std::make_unique<OperationExpression>(std::move(resultExpression), std::move(newExpression), currentOperation);
                }
                expectingOperator = true;
                currentOperation = none;
                return true;
            }
        }

        return false;
    }, [&](){
        return done;
    });
}



bool Interpreter::read_o_toki_e_ni() {
    std::unique_ptr<std::vector<std::unique_ptr<Token>>> requiredTokens = std::make_unique<std::vector<std::unique_ptr<Token>>>();
    requiredTokens->push_back(std::make_unique<WordToken>("o"));
    requiredTokens->push_back(std::make_unique<WordToken>("toki"));
    requiredTokens->push_back(std::make_unique<WordToken>("e"));
    requiredTokens->push_back(std::make_unique<WordToken>("ni"));
    requiredTokens->push_back(std::make_unique<ColonToken>());
    return read_tokens(std::move(requiredTokens));
}
