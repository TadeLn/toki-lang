#include "OperationExpression.hpp"

#include <stdexcept>



std::unique_ptr<Value> OperationExpression::eval() {
    std::unique_ptr<Value> va = this->a->eval();
    std::unique_ptr<Value> vb = this->b->eval();

    switch (this->op) {
        case Operation::ADD:
            return va->add(vb);
        case Operation::SUBTRACT:
            return va->subtract(vb);
        case Operation::MULTIPLY:
            return va->multiply(vb);
        case Operation::DIVIDE:
            return va->divide(vb);
        default:
            throw std::runtime_error("invalid operation");
    }
}

OperationExpression::OperationExpression(std::unique_ptr<Expression> a, std::unique_ptr<Expression> b, OperationExpression::Operation op) {
    this->a = std::move(a);
    this->b = std::move(b);
    this->op = op;
}