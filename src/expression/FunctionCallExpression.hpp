#pragma once

#include "Expression.hpp"

#include <vector>



class FunctionCallExpression : public Expression {

public:
    std::string name;
    std::vector<std::unique_ptr<Expression>> args;

    std::unique_ptr<Value> eval();
    FunctionCallExpression(std::string name);

};