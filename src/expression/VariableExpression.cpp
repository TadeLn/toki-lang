#include "VariableExpression.hpp"

#include "../value/IntValue.hpp"



std::unique_ptr<Value> VariableExpression::eval() {
    // [TODO] get actual variable value
    return std::make_unique<IntValue>(0);
}

VariableExpression::VariableExpression(std::string name) {
    this->name = name;
}