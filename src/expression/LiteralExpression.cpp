#include "LiteralExpression.hpp"



std::unique_ptr<Value> LiteralExpression::eval() {
    return std::move(this->value);
}

LiteralExpression::LiteralExpression(std::unique_ptr<Value> value) {
    this->value = std::move(value);
}
