#pragma once

#include "Expression.hpp"



class VariableExpression : public Expression {

public:
    std::string name;

    std::unique_ptr<Value> eval();
    VariableExpression(std::string name);
};
