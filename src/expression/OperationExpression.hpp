#pragma once

#include "Expression.hpp"



class OperationExpression : public Expression {

public:
    enum Operation {
        NONE = -1,
        ADD = 0,
        SUBTRACT,
        MULTIPLY,
        DIVIDE
    };
    
    std::unique_ptr<Expression> a;
    std::unique_ptr<Expression> b;
    Operation op;

    std::unique_ptr<Value> eval();
    OperationExpression(std::unique_ptr<Expression> a, std::unique_ptr<Expression> b, OperationExpression::Operation op);
    virtual ~OperationExpression() = default;
};
