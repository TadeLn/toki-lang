#pragma once

#include <memory>

#include "Expression.hpp"
#include "../value/Value.hpp"



class LiteralExpression : public Expression {

public:
    std::unique_ptr<Value> eval();
    LiteralExpression(std::unique_ptr<Value> value);
    virtual ~LiteralExpression() = default;

private:
    std::unique_ptr<Value> value;

};