#include "FunctionCallExpression.hpp"

#include "../value/IntValue.hpp"



std::unique_ptr<Value> FunctionCallExpression::eval() {
    // [TODO] get actual function return value
    return std::make_unique<IntValue>(0);
}

FunctionCallExpression::FunctionCallExpression(std::string name) {
    this->name = name;
}