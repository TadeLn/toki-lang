#pragma once

#include <memory>

#include "../value/Value.hpp"



class Expression {

public:
    virtual std::unique_ptr<Value> eval() = 0;
    virtual ~Expression() = default;

};
