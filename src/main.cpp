#include <iostream>
#include <vector>
#include <string>

#include "Parser.hpp"
#include "Interpreter.hpp"



int main(int argc, char** argv) {
    // Load args into a vector
    std::vector<std::string> args;
    args.reserve(argc);
    for (int i = 0; i < argc; i++) {
        args.push_back(std::string(argv[i]));
    }

    #ifdef DEBUG
    // Print out args
    std::cout << "Args:\n";
    for (auto it : args) {
        std::cout << "  " << it << "\n";
    }
    #endif

    // Process files from args
    if (args.size() > 1) {
        for (unsigned int i = 1; i < args.size(); i++) {
            #ifdef DEBUG
            std::cout << "Processing file " << i << " out of " << (args.size() - 1) << ": " << args[i] << " \n";
            #endif
            try {
                std::unique_ptr<const std::vector<std::unique_ptr<Token>>> tokens = Parser::parseFile(args[i]);

                Interpreter interpreter = Interpreter(std::move(tokens));
                interpreter.run();
            } catch (std::exception& e) {
                std::cerr << "Uncaught exception: " << e.what() << "\n";
            }
        }
    } else {
        #ifdef DEBUG
        std::cerr << "Error: No input files\n";
        #endif
        return 1;
    }

    return 0;
}
