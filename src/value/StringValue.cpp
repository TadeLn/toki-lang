#include "StringValue.hpp"

#include <memory>
#include <stdexcept>

#include "BoolValue.hpp"
#include "CharValue.hpp"
#include "IntValue.hpp"
#include "FloatValue.hpp"



StringValue::StringValue(std::string value) : SimpleValue(value) {}
StringValue::StringValue() : SimpleValue() {}



// [TODO] operation functions
std::unique_ptr<Value> StringValue::add(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    switch (type) {
        case STRING:
        {
            auto stringValue = static_cast<StringValue*>(v.get());
            return Value::from(this->value + stringValue->value);
        }

        default:
            invalidOperationArgs(type, "ADD");
            return nullptr;
    }
}

std::unique_ptr<Value> StringValue::subtract(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "SUBTRACT");
    return nullptr;
}

std::unique_ptr<Value> StringValue::multiply(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "MULTIPLY");
    return nullptr;
}

std::unique_ptr<Value> StringValue::divide(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "DIVIDE");
    return nullptr;
}

std::string StringValue::toString() const {
    return this->value;
}