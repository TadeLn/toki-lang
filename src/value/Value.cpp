#include "Value.hpp"

#include <stdexcept>

#include "BoolValue.hpp"
#include "CharValue.hpp"
#include "IntValue.hpp"
#include "FloatValue.hpp"
#include "StringValue.hpp"



Value::Type Value::getType() const {
    return this->type;
}



std::string Value::typeToString(Value::Type type) {
    switch (type) {
        case Type::BOOL:   return "BOOL";
        case Type::CHAR:   return "CHAR";
        case Type::INT:    return "INT";
        case Type::FLOAT:  return "FLOAT";
        case Type::STRING: return "STRING";
        case Type::ARRAY:  return "ARRAY";
        case Type::OBJECT: return "OBJECT";
        default:           return "INVALID";
    }
}



std::unique_ptr<Value> Value::from(bool value) {
    return std::make_unique<BoolValue>(value);
}

std::unique_ptr<Value> Value::from(char value) {
    return std::make_unique<CharValue>(value);
}

std::unique_ptr<Value> Value::from(std::int64_t value) {
    return std::make_unique<IntValue>(value);
}

std::unique_ptr<Value> Value::from(std::uint64_t value) {
    return std::make_unique<IntValue>(value);
}

std::unique_ptr<Value> Value::from(double value) {
    return std::make_unique<FloatValue>(value);
}

std::unique_ptr<Value> Value::from(std::string value) {
    return std::make_unique<StringValue>(value);
}



Value::Value(Value::Type type) {
    this->type = type;
}

void Value::invalidOperationArgs(Value::Type type2, std::string operation) const {
    Type type1 = this->getType();
    throw std::runtime_error(std::string() + "invalid operation: type: " + operation + ", args: " + typeToString(type1) + " and " + typeToString(type2));
}

