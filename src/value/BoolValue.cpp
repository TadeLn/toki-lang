#include "BoolValue.hpp"

#include <memory>
#include <stdexcept>

#include "CharValue.hpp"
#include "IntValue.hpp"
#include "FloatValue.hpp"
#include "StringValue.hpp"



BoolValue::BoolValue(bool value) : SimpleValue(value) {}
BoolValue::BoolValue() : SimpleValue() {}



// [TODO] operation functions
std::unique_ptr<Value> BoolValue::add(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "ADD");
    return nullptr;
}

std::unique_ptr<Value> BoolValue::subtract(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "SUBTRACT");
    return nullptr;
}

std::unique_ptr<Value> BoolValue::multiply(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "MULTIPLY");
    return nullptr;
}

std::unique_ptr<Value> BoolValue::divide(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "DIVIDE");
    return nullptr;
}

std::string BoolValue::toString() const {
    return this->value ? "lon" : "lon ala";
}