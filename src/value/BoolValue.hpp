#pragma once

#include "SimpleValue.hpp"



class BoolValue : public SimpleValue<bool, Value::Type::BOOL> {

public:
    BoolValue(bool value);
    BoolValue();

    std::unique_ptr<Value> add(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> subtract(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> multiply(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> divide(const std::unique_ptr<Value>& v) const;
    std::string toString() const;

};
