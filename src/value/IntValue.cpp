#include "IntValue.hpp"

#include <memory>
#include <stdexcept>

#include "BoolValue.hpp"
#include "CharValue.hpp"
#include "FloatValue.hpp"
#include "StringValue.hpp"



IntValue::IntValue(std::int64_t value) : SimpleValue(value) {}
IntValue::IntValue() : SimpleValue() {}



// [TODO] operation functions
std::unique_ptr<Value> IntValue::add(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "ADD");
    return nullptr;
}

std::unique_ptr<Value> IntValue::subtract(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "SUBTRACT");
    return nullptr;
}

std::unique_ptr<Value> IntValue::multiply(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "MULTIPLY");
    return nullptr;
}

std::unique_ptr<Value> IntValue::divide(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "DIVIDE");
    return nullptr;
}

std::string IntValue::toString() const {
    return std::to_string(this->value);
}