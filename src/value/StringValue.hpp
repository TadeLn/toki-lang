#pragma once

#include <string>

#include "SimpleValue.hpp"



class StringValue : public SimpleValue<std::string, Value::Type::STRING> {

public:
    StringValue(std::string value);
    StringValue();

    std::unique_ptr<Value> add(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> subtract(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> multiply(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> divide(const std::unique_ptr<Value>& v) const;
    std::string toString() const;

};
