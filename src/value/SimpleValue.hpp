#pragma once

#include "Value.hpp"



template<typename T, auto U>
class SimpleValue : public Value {

public:
    T value;

    SimpleValue(T value)
        : SimpleValue() {
        this->value = value;
    }
    
    SimpleValue() : Value(U) {}

    virtual std::unique_ptr<Value> add(const std::unique_ptr<Value>& v) const = 0;
    virtual std::unique_ptr<Value> subtract(const std::unique_ptr<Value>& v) const = 0;
    virtual std::unique_ptr<Value> multiply(const std::unique_ptr<Value>& v) const = 0;
    virtual std::unique_ptr<Value> divide(const std::unique_ptr<Value>& v) const = 0;
    virtual std::string toString() const = 0;
};

