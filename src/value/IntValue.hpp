#pragma once

#include "SimpleValue.hpp"



class IntValue : public SimpleValue<std::int64_t, Value::Type::INT> {

public:
    IntValue(std::int64_t value);
    IntValue();

    std::unique_ptr<Value> add(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> subtract(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> multiply(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> divide(const std::unique_ptr<Value>& v) const;
    std::string toString() const;

};
