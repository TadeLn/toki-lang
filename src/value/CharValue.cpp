#include "CharValue.hpp"

#include <memory>
#include <stdexcept>

#include "BoolValue.hpp"
#include "IntValue.hpp"
#include "FloatValue.hpp"
#include "StringValue.hpp"



CharValue::CharValue(char value) : SimpleValue(value) {}
CharValue::CharValue() : SimpleValue() {}



// [TODO] operation functions
std::unique_ptr<Value> CharValue::add(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "ADD");
    return nullptr;
}

std::unique_ptr<Value> CharValue::subtract(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "SUBTRACT");
    return nullptr;
}

std::unique_ptr<Value> CharValue::multiply(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "MULTIPLY");
    return nullptr;
}

std::unique_ptr<Value> CharValue::divide(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "DIVIDE");
    return nullptr;
}

std::string CharValue::toString() const {
    return std::string(1, this->value);
}