#pragma once

#include "SimpleValue.hpp"



class CharValue : public SimpleValue<char, Value::Type::CHAR> {

public:
    CharValue(char value);
    CharValue();

    std::unique_ptr<Value> add(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> subtract(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> multiply(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> divide(const std::unique_ptr<Value>& v) const;
    std::string toString() const;

};
