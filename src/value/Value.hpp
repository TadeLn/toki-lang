#pragma once

#include <string>
#include <memory>



class Value {

public:
    enum Type {
        BOOL,
        CHAR,
        INT,
        FLOAT,
        STRING,
        ARRAY,
        OBJECT
    };

    Type getType() const;

    virtual std::unique_ptr<Value> add(const std::unique_ptr<Value>& v) const = 0;
    virtual std::unique_ptr<Value> subtract(const std::unique_ptr<Value>& v) const = 0;
    virtual std::unique_ptr<Value> multiply(const std::unique_ptr<Value>& v) const = 0;
    virtual std::unique_ptr<Value> divide(const std::unique_ptr<Value>& v) const = 0;
    virtual std::string toString() const = 0;

    static std::string typeToString(Type type);

    static std::unique_ptr<Value> from(bool value);
    static std::unique_ptr<Value> from(char value);
    static std::unique_ptr<Value> from(std::int64_t value);
    static std::unique_ptr<Value> from(std::uint64_t value);
    static std::unique_ptr<Value> from(double value);
    static std::unique_ptr<Value> from(std::string value);
    
    virtual ~Value() = default;

protected:
    Value(Type type);

    void invalidOperationArgs(Type type, std::string operation) const;

private:
    Type type;

};
