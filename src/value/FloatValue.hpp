#pragma once

#include "SimpleValue.hpp"



class FloatValue : public SimpleValue<double, Value::Type::FLOAT> {

public:
    FloatValue(double value);
    FloatValue();

    std::unique_ptr<Value> add(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> subtract(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> multiply(const std::unique_ptr<Value>& v) const;
    std::unique_ptr<Value> divide(const std::unique_ptr<Value>& v) const;
    std::string toString() const;

};
