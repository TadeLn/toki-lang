#include "FloatValue.hpp"

#include <memory>
#include <stdexcept>

#include "BoolValue.hpp"
#include "CharValue.hpp"
#include "IntValue.hpp"
#include "StringValue.hpp"



FloatValue::FloatValue(double value) : SimpleValue(value) {}
FloatValue::FloatValue() : SimpleValue() {}



// [TODO] operation functions
std::unique_ptr<Value> FloatValue::add(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "ADD");
    return nullptr;
}

std::unique_ptr<Value> FloatValue::subtract(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "SUBTRACT");
    return nullptr;
}

std::unique_ptr<Value> FloatValue::multiply(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "MULTIPLY");
    return nullptr;
}

std::unique_ptr<Value> FloatValue::divide(const std::unique_ptr<Value>& v) const {
    Type type = v->getType();
    invalidOperationArgs(type, "DIVIDE");
    return nullptr;
}

std::string FloatValue::toString() const {
    return std::to_string(this->value);
}